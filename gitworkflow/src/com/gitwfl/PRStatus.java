package com.gitwfl;

public interface PRStatus {
    void next(PR status);
    void prev(PR status);
    void printStatus();
}
