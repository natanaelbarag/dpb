package com.gitwfl;

public class Main {

    public static void main(String[] args) {
        PR pullRequest = new PR();
        pullRequest.printStatus();
        pullRequest.nextStatus();
        pullRequest.printStatus();
        pullRequest.nextStatus();
        pullRequest.printStatus();
        pullRequest.previousStatus();
        pullRequest.printStatus();
        pullRequest.nextStatus();
        pullRequest.printStatus();
        pullRequest.nextStatus();
        pullRequest.printStatus();
        pullRequest.nextStatus();
        pullRequest.previousStatus();

    }
}
