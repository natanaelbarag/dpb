package com.gitwfl;

public class WaitingForAuthorStatus implements PRStatus {

    @Override
    public void next(final PR pr) {
        String setTo = "rejected";
        if(setTo.equals("rejected"))
            pr.status = new RejectedStatus();
        else if(setTo.equals("approved"))
            pr.status = new ApprovedStatus();
    }

    @Override
    public void prev(final PR pr) {
        pr.status = new InReviewStatus();
    }

    @Override
    public void printStatus() {
        System.out.println("waiting for author");
    }
}
