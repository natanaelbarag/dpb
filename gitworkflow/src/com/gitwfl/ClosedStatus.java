package com.gitwfl;

public class ClosedStatus implements PRStatus {

    @Override
    public void next(final PR status) {
        System.out.println("already closed");
    }

    @Override
    public void prev(final PR status) {
        System.out.println("already closed, you can not do anything more");
    }

    @Override
    public void printStatus() {
        System.out.println("closed");
    }
}
