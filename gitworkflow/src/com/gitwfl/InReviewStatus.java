package com.gitwfl;

public class InReviewStatus implements PRStatus {

    @Override
    public void next(final PR pr) {
        pr.status = new WaitingForAuthorStatus();
    }

    @Override
    public void prev(final PR status) {
        System.out.println("it is already in review state, can not go back");
    }

    @Override
    public void printStatus() {
        System.out.println("in review");
    }
}
