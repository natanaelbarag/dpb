package com.gitwfl;

public class ApprovedStatus implements PRStatus{


    @Override
    public void next(final PR pr) {
        pr.status = new ClosedStatus();
    }

    @Override
    public void prev(final PR pr) {
    pr.status = new WaitingForAuthorStatus();
    }

    @Override
    public void printStatus() {
        System.out.println("approved");
    }
}
