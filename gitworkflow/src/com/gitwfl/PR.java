package com.gitwfl;

public class PR {
    PRStatus status = new InReviewStatus();

    public void previousStatus(){
        status.prev(this);
    }

    public void nextStatus(){
        status.next(this);
    }

    public void printStatus(){
        status.printStatus();
    }
}
