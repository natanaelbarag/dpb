package com.gitwfl;

import com.gitwfl.Melody;

public interface MusicStatus {
    void next(Melody melody);
    void prev(Melody melody);
    void printStatus();
}
