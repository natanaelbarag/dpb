package com.gitwfl;

public class StopStatus implements MusicStatus {
    @Override
    public void next(final Melody melody) {
        melody.status = new StartState();
    }

    @Override
    public void prev(final Melody melody) {
        System.out.println("already in stopped status");
    }

    @Override
    public void printStatus() {
        System.out.println("stopped");
    }
}
