package com.gitwfl;

public class Melody {
    MusicStatus status = new StopStatus();

    public void printStatus() {
        status.printStatus();
    }

    public void nextStatus() {
        status.next(this);
    }

    public void previousStatus() {
        status.prev(this);
    }
}
