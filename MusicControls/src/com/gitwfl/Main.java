package com.gitwfl;

public class Main {

    public static void main(String[] args) {

        final Melody melody = new Melody();

        melody.previousStatus();
        melody.printStatus();
        melody.nextStatus();
        melody.printStatus();
        melody.nextStatus();
        melody.printStatus();
        melody.nextStatus();
        melody.printStatus();
        melody.nextStatus();
        melody.printStatus();
    }
}
