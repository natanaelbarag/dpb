package com.gitwfl;

public class ResumeState implements MusicStatus {
    @Override
    public void next(final Melody melody) {
        String setTo = "pause";
        if(setTo.equals("pause"))
            melody.status = new PauseState();
        else if(setTo.equals("stop"))
            melody.status = new StopStatus();
    }

    @Override
    public void prev(final Melody melody) {
        melody.status = new PauseState();
    }

    @Override
    public void printStatus() {
        System.out.println("resumed");
    }
}
