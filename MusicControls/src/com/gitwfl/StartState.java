package com.gitwfl;

public class StartState implements MusicStatus {
    @Override
    public void next(final Melody melody) {
        String setTo = "pause";
        if(setTo.equals("pause"))
            melody.status = new PauseState();
        else if(setTo.equals("stop"))
            melody.status = new StopStatus();
    }

    @Override
    public void prev(final Melody melody) {
            melody.status = new StopStatus();
    }

    @Override
    public void printStatus() {
        System.out.println("start");
    }
}
