package com.gitwfl;

public class PauseState implements MusicStatus {
    @Override
    public void next(final Melody melody) {
        melody.status = new ResumeState();
    }

    @Override
    public void prev(final Melody melody) {
        melody.status = new StartState();
    }

    @Override
    public void printStatus() {
        System.out.println("paused");
    }
}
